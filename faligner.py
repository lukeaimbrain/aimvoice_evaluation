import os, time, csv
import librosa, dtw
import numpy as np

#helper for reading anchor csv
def read_anchor_csv(fp):
    frags=[]
    anchors=[]
    with open(fp, 'rb') as f:
        csvreader = csv.reader(f)
        for els in csvreader:
            frag = els[0]
            start = float(els[1])
            frags.append(frag)
            anchors.append(start)
    return frags,anchors

#helpers for transforming through time units (milliseconds, frames, hop)
def frames2hop(frames, hop_length=512):
    '''go from frame level to hop level (for mel spectrograms)'''
    return [f/hop_length for f in frames]
def hop2frames(hops, hop_length=512):
    '''go from frame level to hop level (for mel spectrograms)'''
    return [f*hop_length for f in hops]
def ms2frames(ms, sr=16000.):
    '''go from milliseconds to frames'''
    return [int(a*sr/1000.) for a in ms]
def frames2ms(frames, sr=16000.):
    '''go from milliseconds to frames'''
    return [int(a*1000/sr) for a in frames]

#---the forced aligner
def faligner(audio_path, 
             template_path, 
             transcript_path, 
             sr=16000.,
             output='data',
             padding=0,
             mel_params={'hop_length': 2048}):
    '''
    audio_path: string to input audio (to be aligned)
    template_path: string to template audio 
    transcript: csv file - each line is a fragment to be segmented e.g.:
        my name is,445
        luke palmer,1045
        ....
    where line[0] is the spoken word(s), and line[1] is the millisecond marker for the beginning of that text
    sr: sample rate (not original, but what resolution to load in this function)
    '''
    
    #checks
    output_formats = ['tran-ms', 'data', 'frames', 'ms']
    if output not in output_formats:
        Exception('output_format not specified, select from {}'.format(output_formats))
    
    #load audio
    eval_data, r = librosa.load(audio_path, sr=sr)
    template_data, r = librosa.load(template_path, sr=sr)
    
    #load annotated transcript
    template_frags, template_anchors = read_anchor_csv(transcript_path)
    
    #extract mfcc
    eval_mfcc = librosa.feature.mfcc(y=eval_data, sr=sr, **mel_params)
    template_mfcc = librosa.feature.mfcc(y=template_data, sr=sr, **mel_params)
    
    #dtw match
    dist, cost, acc, (eval_path, temp_path) = \
        dtw.dtw(eval_mfcc.T, template_mfcc.T, dist=lambda x,y: np.linalg.norm(x - y, ord=2))
    
    #find corresponding anchor points in hop space!
    template_anchors_frames = ms2frames(template_anchors, sr=sr)
    template_anchors_hop = frames2hop(template_anchors_frames, hop_length=mel_params['hop_length'])
    eval_inds = [np.where(temp_path==a)[0][0] for a in template_anchors_hop]
    eval_anchors_hop = [eval_path[ind] for ind in eval_inds]
    
    #translate back up to ms
    eval_anchors_frames = hop2frames(eval_anchors_hop, hop_length=mel_params['hop_length'])
    eval_anchors_ms = frames2ms(eval_anchors_frames, sr=sr)
    
    #create frag-ms tuples
    eval_aligned = [(frag, anchor) for frag, anchor in zip(template_frags, eval_anchors_ms)]
    
    #deal with outputs
    if output == 'ms':
        return eval_anchors_ms
    elif output == 'frames':
        return eval_anchors_frames
    elif output == 'tran-ms':
        return eval_aligned
    elif output == 'data':
        p = int(padding*sr/1000.) #turn padding amount into frame unit
        range_inds = eval_anchors_frames + [len(eval_data)] #to get end of range
        range_inds[0] = p if range_inds[0] < p else range_inds[0] #bounds check, since using as index
        data = [eval_data[range_inds[ind]-p : range_inds[ind+1]+p] \
                for ind in range(len(eval_anchors_frames))]
        return data